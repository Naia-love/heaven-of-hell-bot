const { Command } = require('klasa');
const client = require('nekos.life');
const { MessageEmbed } = require('discord.js');

module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			aliases: ['lovecum', "cumslut"],
			description: language => language.get('HELP_CUMSLUTS'),
      nsfw: true
		});
	}

  async run(msg) {
		msg.delete();
    const neko = new client();
    let gif = await neko.nsfw.cumsluts();
    const e = new MessageEmbed()
      .setTitle("Cumslut!")
      .setURL(gif.url)
      .setImage(gif.url)
			.setFooter(`Requested by ${msg.author.tag}`, msg.author.displayAvatarURL())
    msg.channel.send(e)
  }
};
