const { Command } = require('klasa');
const client = require('nekos.life');

module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			aliases: ['catemote', 'catemoji'],
			description: language => language.get('HELP_CATMOJ')
		});
	}

	async run(msg) {
		msg.delete();
		const { channel } = msg;
		const neko = new client();
		const cat = await neko.sfw.catText();
		let webhooks = await channel.fetchWebhooks();
		if (!webhooks.first()) {
			channel.createWebhook('owo');
			webhooks = await channel.fetchWebhooks();
		}
		const webhook = webhooks.first();
		await webhook.send(cat.cat, {
			username: msg.author.username,
			avatarURL: msg.author.displayAvatarURL()
		});
	}

};
