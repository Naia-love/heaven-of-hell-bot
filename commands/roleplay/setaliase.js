const { Command } = require('klasa');

module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			aliases: ['aliase'],
			runIn: ['text'],
			description: `add aliases to your ocs! Use like \`hh!aliase *id* *mynewaliase*\`\n*oc = your oc's id (\`hh!me\`)*\n After use your aliase like \`hh:*youraliase* text.\``,
			usage: '[oc:integer] <aliase:str{1,10}>',
			usageDelim: ' '
		});
	}
	async run(msg, [oc = 0, aliase]) {
		const user = msg.author;
		await user.settings.sync();
		const ocs = await user.settings.get('OC');
		const aliases = await user.settings.get('aliase');
		if (oc >= ocs.length || ocs.length === 0) return msg.channel.send('Oop, you don\'t have an OC with this ID.');
		/*
		 *for (let i; i < ocs.length; i++) {
		 *	if ( aliase.toLowerCase() === aliases[i].aliase ) return msg.channel.send('You already use this aliase!\nSee `hh!help editaliase`');
		 *}
		 *
		 *aliases.forEach(element => {
		 *	if (element.aliase === aliase.toLowerCase()) return msg.channel.send('You already use this aliase!\nSee `hh!help editaliase`');
		 *});
		 */
		if (aliases.some(element => element.aliase === aliase.toLowerCase())) return msg.channel.send('You already use this aliase!\nSee `hh!help editaliase`');
		user.settings.update('aliase', { oc, aliase: aliase.toLowerCase() }, { arrayAction: 'add' });
		msg.channel.send(`You created the alisase \`${aliase}\` for your oc \`${ocs[oc].name}\` (ID:\`${oc}\`).`);
	}

};
