const { Command } = require('klasa');
const client = require('nekos.life');
const { MessageEmbed } = require('discord.js');

module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			aliases: ['hugs', 'huggu', 'hugg', 'huggs'],
			description: language => language.get('HELP_HUG'),
      usage: '[user:user]'
		});
	}

  async run(msg, [user]) {
		msg.delete();
    const hugs = [
      `${msg.author} hugs tight ${user}!`,
      `${msg.author} wrap their arms around ${user}!`,
      `Are you oki ${user}? Well, ${msg.author} is here for you!`,
      `Awwww, i think ${msg.author} like you ${user}! They are huggin you uwu`
    ];
    const neko = new client();
    let gif = await neko.sfw.hug();
    if (!user) {
      const e = new MessageEmbed()
        .setTitle('Gif of hugssss!')
        .setURL(gif.url)
        .setImage(gif.url)
        .setFooter(`Requested by ${msg.author.tag}`, msg.author.displayAvatarURL())
      msg.channel.send(e);
    }
    else {
      const e = new MessageEmbed()
        .setDescription(user === msg.author
				      ? (`Awww... here a hug for you ${msg.author}!`)
				      : (hugs[Math.floor(Math.random() * hugs.length)]))
        .setImage(gif.url)
        .setFooter(`Requested by ${msg.author.tag}`, msg.author.displayAvatarURL());
      msg.channel.send(e);
    }
  }
};
