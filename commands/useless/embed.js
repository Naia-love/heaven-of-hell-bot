const { Command } = require('klasa');


module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			description: 'embed',
			usage: '[channel:channel]',
			permissionLevel: 10,
			usageDelim: ' '
		});
	}

	async run(msg, [channel = msg.channel]) {
		msg.delete();
		const embed = {
			title: 'Useful tips for Heaven of hell',
			description: 'Heaven of hell bot, created especially for this server, have a lot of functions to improve your rp/erp *that are still in dev* \n**But, for most of those commands, you must have at least one character aproved**\nAlso, remember that hh!help exist ;)',
			author: {
				name: 'heaven of Hell',
				url: 'https://gitlab.com/Naia-love/heaven-of-hell-bot/issues',
				icon_url: 'https://cdn.discordapp.com/attachments/700760728243798056/709175235307634728/image0.gif'
			},
			color: 10027247,
			footer: { text: 'Keep in mind the bot is still in dev, and so may be bugged.' },
			fields: [
				{
					name: 'hh!talk (id) *text*',
					value: 'The basic command to talk, if you have more than one OC, use like that: `hh!talk *id* text` ',
					inline: false
				},
				{
					name: 'hh!aliase (id) *aliase*',
					value: 'You can set an aliases! You just have to write `hh!aliase *id* youraliase` :D. You can have more than one aliases per OCs of course :P',
					inline: false
				},
				{
					name: 'hh:*aliase* *text*',
					value: "So, how to use aliases? You just have to write `hh:*youraliase* text` and it's magic!",
					inline: false
				},
				{
					name: 'hh!editaliase *aliase* (id|aliase|delete) *new value*',
					value: 'to modify an aliases! Do `hh!editaliase *youraliase* aliase *newaliase*` to edit the aliase, also `hh!editaliase *youraliase* id *newid*` to change the ID of the oc of the aliase (or `hh!editaliase *youraliase* delete` to delete)',
					inline: false
				},
				{
					name: 'hh!me (or hh!i or hh!profile)',
					value: 'To see your *or someone* curent OCs, their avatar, their description, and their aliases!.',
					inline: false
				},
				{
					name: 'and modify my OC?',
					value: "You can't, but you can just ask an admin to do it for you :D",
					inline: false
				},
				{
					name: 'I have an idea for the bot!',
					value: 'Cool! you can check the [gitlab](https://gitlab.com/Naia-love/heaven-of-hell-bot/-/issues) or contact <@644633142828990505>',
					inline: false
				}
			]
		};
		channel.send({ embed });
	}

};
