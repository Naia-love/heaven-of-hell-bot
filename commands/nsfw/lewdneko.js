const { Command } = require('klasa');
const client = require('nekos.life');
const { MessageEmbed } = require('discord.js');
const neko = new client();

module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
      aliases:['lewdnekomimi', 'nekolewd'],
			description: language => language.get('HELP_LEWDNEKO'),
      nsfw: true
		});
	}

  async run(msg) {
		msg.delete();
    let gif = await nekos[Math.floor(Math.random() * nekos.length)]()
    const e = new MessageEmbed()
      .setTitle("Lewd neko~")
      .setURL(gif.url)
      .setImage(gif.url)
			.setFooter(`Requested by ${msg.author.tag}`, msg.author.displayAvatarURL())
    msg.channel.send(e);
  }
};

const nekos = [
  neko.nsfw.neko,
  neko.nsfw.nekoGif,
  neko.nsfw.eroNeko
];
