const { Command } = require('klasa');
const client = require('nekos.life');
const { MessageEmbed } = require('discord.js');
const neko = new client();

module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
      aliases:['nekomimi'],
			description: language => language.get('HELP_NEKO')
		});
	}

  async run(msg) {
		msg.delete();
    let gif = await nekos[Math.floor(Math.random() * nekos.length)]();
    const e = new MessageEmbed()
      .setTitle("Some cute neko :3")
      .setURL(gif.url)
      .setImage(gif.url)
			.setFooter(`Requested by ${msg.author.tag}`, msg.author.displayAvatarURL());
    msg.channel.send(e);
  }
};

const nekos = [
  neko.sfw.neko,
  neko.sfw.nekoGif
]
