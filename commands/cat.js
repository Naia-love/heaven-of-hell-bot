const { Command } = require('klasa');
const fetch = require('node-fetch');
const { MessageEmbed } = require('discord.js');

module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			aliases: ['randomcat', 'meow'],
			description: 'Grabs a random cat image from random.cat.'
		});
	}

	async run(msg) {
		msg.delete();
		const file = await fetch('http://aws.random.cat/meow')
			.then(response => response.json())
			.then(body => body.file);
		const e = new MessageEmbed()
			.setTitle('Some cat!')
			.setURL(file)
			.setImage(file)
			.setFooter(`Requested by ${msg.author.tag}`, msg.author.displayAvatarURL());
		msg.channel.send(e);
		// return msg.channel.sendFile(file, `cat.${file.slice(file.lastIndexOf('.'), file.length)}`);
	}

};
