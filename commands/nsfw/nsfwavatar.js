const { Command } = require('klasa');
const client = require('nekos.life');
const { MessageEmbed } = require('discord.js');

module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			aliases: ['lewdavatar', 'lewda','nsfwa'],
			description: language => language.get('HELP_NSFW_AVATAR'),
      nsfw: true
		});
	}

  async run(msg) {
		msg.delete();
    const neko = new client();
    let gif = await neko.nsfw.avatar();
    const e = new MessageEmbed()
      .setTitle("Here some lewd avatar :P")
      .setURL(gif.url)
      .setImage(gif.url)
			.setFooter(`Requested by ${msg.author.tag}`, msg.author.displayAvatarURL())
    msg.channel.send(e);
  }
};
