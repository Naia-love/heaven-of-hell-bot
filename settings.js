const { Client } = require('klasa');

Client.defaultGuildSchema
	.add('rp', 'string', { default: 'hh:' });

Client.defaultUserSchema
	.add('OC', 'any', { array: true, default: [] })
	.add('aliase', 'any', { array: true, default: [] });
