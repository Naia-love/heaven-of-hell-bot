const { Command } = require('klasa');


module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			description: 'Say something',
			usage: '[channel:channel] <text:...str>',
			permissionLevel: 6,
			usageDelim: ' '
		});
	}

	async run(msg, [channel = msg.channel, text]) {
		msg.delete();
		channel.send(text);
	}

};
