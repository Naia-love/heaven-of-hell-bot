const { Command } = require('klasa');

module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			aliases: ['rpprefix'],
			cooldown: 5,
			runIn: ['text'],
			description: 'the prefix for rp',
			usage: '[reset|prefix:str{1,10}]',
			usageDelim: ' '
		});
	}
	async run(message, [prefix]) {
		if (!prefix) return message.send(`The prefix for this guild is \`${message.guild.settings.rp}\``);
		if (!await message.hasAtLeastPermissionLevel(6)) throw message.language.get('INHIBITOR_PERMISSIONS');
		if (prefix === 'reset') return this.reset(message);
		if (message.guild.settings.rp === prefix) throw message.language.get('CONFIGURATION_EQUALS');
		await message.guild.settings.update('rp', prefix);
		return message.send(`The prefix for this guild has been set to \`${prefix}\``);
	}

	async reset(message) {
		await message.guild.settings.reset('rp');
		return message.send(`Switched back the guild's prefix back to \`hh:\`!`);
	}

};
