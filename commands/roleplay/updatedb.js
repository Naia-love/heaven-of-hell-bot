const { Command } = require('klasa');

module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			aliases: ['updateuser'],
			description: 'update the oc of an user (arg: name, url, pfp, delete)',
			usage: '<user:user> [id:integer] <arg:str> [modify:...str]',
			usageDelim: ' ',
			runIn: ['text'],
			permissionLevel: 6
		});
	}
	async run(msg, [user, id = 0, arg, modify]) {
		await user.settings.sync();
		const i = user.settings.get('OC');
		if (arg.toLowerCase() === 'name') {
			user.settings.update('OC', { name: modify, profilePic: i[id].profilePic, pathToProfile: i[id].pathToProfile }, { arrayPosition: id });
			await user.settings.sync([true]);
			const up = user.settings.get('OC');
			user.send(`${msg.author.tag} modified your OCs.\n\nChanged name of your OC:\nid: \`${id}\`\nold name : \`${up[id].name}\` new name \`${i[id].name}\``);
			msg.channel.send(`Succefuly modified the OCs of ${user.tag}\n\nChanged name of OC:\nid: \`${id}\`\nold name : \`${up[id].name}\` new name \`${i[id].name}\``);
		} else if (arg.toLowerCase() === 'url') {
			user.settings.update('OC', { name: i[id].name, profilePic: i[id].profilePic, pathToProfile: modify }, { arrayPosition: id });
			await user.settings.sync([true]);
			const up = user.settings.get('OC');
			user.send(`${msg.author.tag} modified your OCs.\n\nChanged the description of your OC:\nid: \`${id}\`\nname : \`${i[id].name}\`\nold desciption : \`${up[id].pathToProfile}\` new desciption \`${i[id].pathToProfile}\``);
			msg.channel.send(`Succefuly modified the OCs of ${user.tag}\n\nChanged the description of OC:\nid: \`${id}\`\nname : \`${i[id].name}\`\nold desciption : \`${up[id].pathToProfile}\` new desciption \`${i[id].pathToProfile}\``);
		} else if (arg.toLowerCase() === 'pfp') {
			user.settings.update('OC', { name: i[id].name, profilePic: modify, pathToProfile: i[id].pathToProfile }, { arrayPosition: id });
			await user.settings.sync([true]);
			const up = user.settings.get('OC');
			user.send(`${msg.author.tag} modified your OCs.\n\nChanged the profile picture of your OC:\nid: \`${id}\`\nname : \`${i[id].name}\`\nold profilePic : \`${up[id].profilePic}\`\nnew profilePic \`${i[id].profilePic}\``);
			msg.channel.send(`Succefuly modified the OCs of ${user.tag}\n\nChanged the profile picture of OC:\nid: \`${id}\`\nname : \`${i[id].name}\`\nold profilePic : \`${up[id].profilePic}\`\nnew profilePic \`${i[id].profilePic}\``);
		} else if (arg.toLowerCase() === 'delete') {
			const del = i.splice(id, 1);
			await user.settings.update('OC', i, { arrayAction: 'overwrite' });
			user.send(`${msg.author.tag} modified your OCs.\n\nDeleted your OC id \`${id}\` name \`${del[0].name}\`\nPlease remember that your OC's ID may have changed, think to edit your aliase (\`hh!help editaliase\`)`);
			msg.channel.send(`Succefuly modified the OCs of ${user.tag}\n\nDeleted the OC id \`${id}\` name \`${del[0].name}\``);
		} else {
			msg.channel.send('you can only change name, ulr (where the descritption button goes) and pfp (the profile picture), or delete');
		}
		msg.delete();
		await user.settings.sync([true]);
	}

};
