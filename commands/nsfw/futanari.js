const { Command } = require('klasa');
const client = require('nekos.life');
const { MessageEmbed } = require('discord.js');

module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			aliases: ['futa'],
			description: language => language.get('HELP_FUTANARI'),
      nsfw: true
		});
	}

  async run(msg) {
		msg.delete();
    const neko = new client();
    let gif = await neko.nsfw.futanari();
    const e = new MessageEmbed()
      .setTitle("Futa? Futa!")
      .setURL(gif.url)
      .setImage(gif.url)
			.setFooter(`Requested by ${msg.author.tag}`, msg.author.displayAvatarURL())
    msg.channel.send(e);
  }
};
