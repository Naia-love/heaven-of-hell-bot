const { translate } = require('google-translate-api-browser');
const { Command } = require('klasa');
const { MessageEmbed } = require('discord.js');

module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			aliases: ['trans'],
			description: language => language.get('HELP_TRANSLATE'),
			usage: '<lang:str> <text:str> [langfrom:str]',
			usageDelim: '|'
		});
	}
	async run(msg, [lang, text, langfrom = 'auto']) {
		const final = await translate(text, { from: langfrom, to: lang });
		const e = new MessageEmbed()
			.setTitle(`Translation from ${final.from.language.iso} to ${lang}`)
			.setDescription(final.text)
			.setFooter(`Requested by ${msg.author.tag}`, msg.author.displayAvatarURL());
		msg.channel.send(e);
	}

};
