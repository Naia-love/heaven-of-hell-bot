const { Command } = require('klasa');

module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			// aliases: [],
			description: language => language.get('HELP_WEBHOOK'),
			usage: '[channel:channel] <text:string> [name:string] [url:url] ',
			permissionLevel: 6,
			usageDelim: '|'
		});
	}

	async run(msg, [channel = msg.channel, text, name = msg.author.username, url = msg.author.displayAvatarURL()]) {
		msg.delete();
		/*
		 * if (!channel) channel = msg.channel;
		 * if (!name) name = msg.author.username;
		 * if (!url) url = msg.author.displayAvatarURL();
		 */
		let webhooks = await channel.fetchWebhooks();
		if (!webhooks.first()) {
			await channel.createWebhook('owo');
			webhooks = await channel.fetchWebhooks();
		}
		const webhook = webhooks.first();
		webhook.send(text, {
			username: name,
			avatarURL: url
		});
	}

};
