const { Command } = require('klasa');

module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			aliases: ['talkdel', 'deltalk', 'deletetalk'],
			runIn: ['text'],
			description: 'Delete your last msg throught hh!talk *precise ID*',
			usage: '<id:str>'
		});
	}

	async run(msg, [id]) {
		await msg.delete();
		const { channel } = msg;
		const user = msg.author;
		const oc = await user.settings.get('OC');
		const themsg = await channel.messages.fetch(id);
		const web = await themsg.fetchWebhook();
		if (oc.length > 0) themsg.delete();
		else channel.send('You can\'t delete this msg');
	}

};
