const { Event } = require('klasa');
const role = '<@&701165558632022177>';

module.exports = class extends Event {

	constructor(...args) {
		super(...args, {
			name: 'welcome after approved',
			enabled: true,
			event: 'guildMemberUpdate',
			once: false
		});
	}

	async run(before = this.oldMember, after = this.newMember) {
		const channel = await this.client.channels.cache.get('700618288031465484');
		const bef = before.roles.cache.array();
		const aft = after.roles.cache.array();
		if (bef.toString().includes(role) === true && aft.toString().includes(role) === false) channel.send(`Welcome ${after.user} to our lustfull server.\nGo grab some <#700681353741729882> !\nAnd have fun :3`);
	}

};
