const { Event } = require('klasa');

module.exports = class extends Event {

	constructor(...args) {
		super(...args, {
			name: 'presence-left',
			enabled: true,
			event: 'guildMemberRemove',
			once: false
		});
	}
	async run() {
		await this.client.user.setPresence({
			activity: {
				name: `${this.client.guilds.cache.reduce((x, y) => x + y.memberCount, 0)} slut.`,
				type: 'LISTENING'
			}
		});
	}

};
