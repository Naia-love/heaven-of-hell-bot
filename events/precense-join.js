const { Event } = require('klasa');

module.exports = class extends Event {

	constructor(...args) {
		super(...args, {
			name: 'presence-join',
			enabled: true,
			event: 'guildMemberAdd',
			once: false
		});
	}
	async run() {
		await this.client.user.setPresence({
			activity: {
				name: `${this.client.guilds.cache.reduce((x, y) => x + y.memberCount, 0)} slut.`,
				type: 'LISTENING'
			}
		});
	}

};
