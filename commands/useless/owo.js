const { Command } = require('klasa');
const client = require('nekos.life');

module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			aliases: ['owofy'],
			description: language => language.get('HELP_OWO'),
			usage: '<text:string>'
		});
	}

	async run(msg, [text]) {
		msg.delete();
		const { channel } = msg;
		const neko = new client();
		const owo = await neko.sfw.OwOify({ text });
		let webhooks = await channel.fetchWebhooks();
		if (!webhooks.first()) {
			channel.createWebhook('owo');
			webhooks = await channel.fetchWebhooks();
		}
		const webhook = webhooks.first();
		await webhook.send(owo.owo, {
			username: msg.author.username,
			avatarURL: msg.author.displayAvatarURL()
		});
	}

};
