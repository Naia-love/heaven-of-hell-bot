// UWU
const { Monitor } = require('klasa');

module.exports = class extends Monitor {

	constructor(...args) {
		super(...args, {
			name: 'talk-handler',
			enabled: true,
			ignoreBots: true,
			ignoreSelf: true,
			ignoreOthers: false,
			ignoreWebhooks: true,
			ignoreEdits: true,
			ignoreBlacklistedUsers: false,
			ignoreBlacklistedGuilds: false
		});
	}

	async run(msg) {
		const { content } = msg;
		if (!content.toLowerCase().startsWith(msg.guild.settings.rp)) return;
		const ocs = await msg.author.settings.get('OC');
		if (ocs.length === 0) return;
		const theOC = content.slice(msg.guild.settings.rp.length).split(' ');
		const aliases = await msg.author.settings.get('aliase');
		let oc;
		let oco;
		for (oco = 0; oco < aliases.length; oco++) {
			if (theOC[0].toLowerCase() === aliases[oco].aliase) oc = aliases[oco].oc;
			else if (oco === aliases.length) return msg.channel.send(`you don't have any OC with the aliase \`${theOC[0].toLowerCase()}\``);
		}
		theOC.shift();
		const text = theOC.join(' ');
		this.command(msg, [text, oc, ocs]);
	}
	async command(msg, [text, oc, ocs]) {
		let webhooks = await msg.channel.fetchWebhooks();
		if (!webhooks.first()) {
			await msg.channel.createWebhook('owo');
			webhooks = await msg.channel.fetchWebhooks();
		}
		const webhook = webhooks.first();
		webhook.send(text, {
			username: ocs[`${oc}`].name,
			avatarURL: ocs[`${oc}`].profilePic
		});
		msg.delete();
	}

};
