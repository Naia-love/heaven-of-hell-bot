const { Command, RichDisplay } = require('klasa');
const { MessageEmbed } = require('discord.js');

module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			aliases: ['me', 'i'],
			cooldown: 5,
			description: 'See someone else profile or your own',
			usage: '[user:user]'
		});
	}
	async run(msg, [user = msg.author]) {
		const i = await user.settings.get('OC');
		const aliase = await user.settings.get('aliase');
		if (i.length > 0) {
			const display = new RichDisplay(new MessageEmbed()
				.setAuthor(`${user.tag}'s OCs`, user.displayAvatarURL())
				.setColor(0x673AB7));
			for (let o = 0; o < i.length; o++) {
				let ioi = [];
				for (let io = 0; io < aliase.length; io++) if (aliase[io].oc === o) ioi.push(`${aliase[io].aliase}`);
				if (!ioi[0]) {
					display.addPage(template => template.setImage(i[`${o}`].profilePic)
						.setTitle(i[`${o}`].name)
						.setDescription(`ID : \`${o}\`\n[description](${i[`${o}`].pathToProfile})\nAliases: No aliase defined`));
				} else {
					display.addPage(template => template.setImage(i[`${o}`].profilePic)
						.setTitle(i[`${o}`].name)
						.setDescription(`ID : \`${o}\`\n[description](${i[`${o}`].pathToProfile})\nAliases: \`${ioi.toString()}\``));
				}
			}
			return display.run(await msg.send('Loading profile'));
		}
		msg.channel.send(`${user} don't have any OCs`);
	}

};
