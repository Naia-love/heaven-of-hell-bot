const { Command } = require('klasa');
const Client = require('nekos.life');
const { MessageEmbed } = require('discord.js');

module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			aliases: ['cuddles'],
			description: language => language.get('HELP_CUDDLE'),
			usage: '[user:user]'
		});
	}

	async run(msg, [user]) {
		msg.delete();
		const cuddles = [
			`${msg.author} kiss ${user}!`,
			`${msg.author} is kissing softly the lips of ${user}!`,
			`Hey ${user}! ${msg.author} is kissing you!`,
			`Awwww, i think ${msg.author} like you ${user}! They are kissing you!`
		];
		const neko = new Client();
		const gif = await neko.sfw.kiss();
		if (!user) {
			const e = new MessageEmbed()
				.setTitle('<a:kissu:707249479979368449>')
				.setURL(gif.url)
				.setImage(gif.url)
				.setFooter(`Requested by ${msg.author.tag}`, msg.author.displayAvatarURL());
			msg.channel.send(e);
		} else {
			const e = new MessageEmbed()
				.setDescription(user === msg.author || user === this.client.user
					? `I- i- >~<\n<a:kissu:707249479979368449>`
					: cuddles[Math.floor(Math.random() * cuddles.length)])
				.setImage(gif.url)
				.setFooter(`Requested by ${msg.author.tag}`, msg.author.displayAvatarURL());
			msg.channel.send(e);
		}
	}

};
