require('dotenv').config();
const { Client } = require('klasa');
require('./settings.js');

new Client({
	fetchAllMembers: false,
	prefix: 'hh!', // modify it to set your own prefix
	commandEditing: true,
	readyMessage: client => `Successfully initialized. Ready to serve ${client.guilds.cache.size} guilds.`,
	shards: 'auto',
	ownerID: `${process.env.OWNER}`, // paste your own id
	commandLogging: true,
	provider: { engine: 'mongodb' }
}).login();
