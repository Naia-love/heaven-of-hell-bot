const { Command } = require('klasa');
// const fetch = require('node-fetch');
const { MessageEmbed } = require('discord.js');

module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			aliases: ['boopnose', 'nose'],
			description: 'Boop!',
			usage: '[user:user]',
		});
	}

	async run(msg, [user]) {
		msg.delete();
		const boop = `https://media1.tenor.com/images/${boops[Math.floor(Math.random() * boops.length)]}/tenor.gif`;
		if (!user) {
			const e = new MessageEmbed()
				.setTitle('Boop!')
				.setURL(boop)
				.setImage(boop)
				.setFooter(`Requested by ${msg.author.tag}`, msg.author.displayAvatarURL());
			msg.channel.send(e);
		}
		else {
			const e = new MessageEmbed()
				.setDescription(user === msg.author
					? (`${msg.author} is booping the nose of ${msg.author}... wait what?!`)
					: (`${msg.author} boop the nose of ${user}!`))
				.setImage(boop)
				.setFooter(`Requested by ${msg.author.tag}`, msg.author.displayAvatarURL());
			msg.channel.send(e);
		}
	}

};

const boops = [
	'a476c822a1d2cd31e9830e18b06e7bf1',
	'27de138dc4ec30784f6d81fed7588142',
	'c8986e82130bc1090e0d73a44870bee4',
	'139810c6a61cf4cb989f1d1bc5c18afa',
	'5ba05529a05a714f93900d79164edca9',
	'9e43855fa64837a186835ec67c649516',
	'e0581b1de9d959925cd8e345d2c54d61',
	'773bd205f1b57800a2164e50b2f4f5fa',
	'afa29e1d381bd863dbd603a765f9bb6b',
	'0557d56de3dcad0674306b3626518fab',
];
