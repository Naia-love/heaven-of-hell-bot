const { Command } = require('klasa');
const client = require('nekos.life');
const { MessageEmbed } = require('discord.js');

module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			aliases: ['plick', 'tongue', 'lick'],
			description: language => language.get('HELP_KUNI'),
      nsfw: true,
      usage: '[user:user]'
		});
	}

  async run(msg, [user]) {
		msg.delete();
    const neko = new client();
    let gif = await neko.nsfw.kuni();
    if (!user) {
      const e = new MessageEmbed()
        .setTitle('A little plick? :P')
        .setURL(gif.url)
        .setImage(gif.url)
        .setFooter(`Requested by ${msg.author.tag}`, msg.author.displayAvatarURL())
      msg.channel.send(e);
    }
    else {
      const e = new MessageEmbed()
        .setDescription(user === msg.author
				      ? (`Wait... ${msg.author} how can you lick your own pussy?`)
				      : (`${msg.author} is licking ${user}'s pussy!`))
        .setImage(gif.url)
        .setFooter(`Requested by ${msg.author.tag}`, msg.author.displayAvatarURL())
      msg.channel.send(e);
    };
  }
};
