//this.client.user.setPresence({ activity: { name: 'with slut :P' }, status: 'idle' })
const { Command } = require('klasa');

module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			permissionLevel: 10,
			guarded: true,
			description: language => language.get('HELP_PRESENCE'),
			usage: '<game:string>'
		});
	}

	async run(msg, [game]) {
		await this.client.user.setPresence({ activity: { name: game } });
		msg.channel.send(`Game changed into ${game}`);
	}
	async init() {
		await this.client.user.setPresence({
			activity: {
				name: `${this.client.guilds.cache.reduce((x, y) => x + y.memberCount, 0)} slut.`,
				type: 'LISTENING'
			}
		});
	}

};
