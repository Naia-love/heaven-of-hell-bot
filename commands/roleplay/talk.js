const { Command } = require('klasa');

module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			aliases: ['tell', 't'],
			cooldown: 5,
			runIn: ['text'],
			description: 'talk with your OCs *oc = your oc\'s id, do hh!me to know yours*',
			usage: '[oc:integer] <text:...str>',
			usageDelim: ' '
		});
	}
	async run(msg, [oc = 0, text]) {
		const i = msg.author.settings.get('OC');
		if (i.length > 0) {
			let webhooks = await msg.channel.fetchWebhooks();
			if (!webhooks.first()) {
				await msg.channel.createWebhook('owo');
				webhooks = await msg.channel.fetchWebhooks();
			}
			const webhook = webhooks.first();
			webhook.send(text, {
				username: i[`${oc}`].name,
				avatarURL: i[`${oc}`].profilePic
			});
			msg.delete();
		} else {
			msg.channel.send('Sorry, you don\'t have any oc');
		}
	}

};
