const { Command } = require('klasa');
const client = require('nekos.life');
const { MessageEmbed } = require('discord.js');

module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			aliases: ['cuddles', 'snuggle', 'snuggles'],
			description: language => language.get('HELP_CUDDLE'),
			usage: '[user:user]',
		});
	}

	async run(msg, [user]) {
		msg.delete();
		const cuddles = [
			`${msg.author} cuddles tight ${user}!`,
			`${msg.author} wrap their arms around ${user}!`,
			`Are you oki ${user}? Well, ${msg.author} is here for you!`,
			`Awwww, i think ${msg.author} like you ${user}! They are cuddling you!`,
		];
		const neko = new client();
		const gif = await neko.sfw.cuddle();
		if (!user) {
			const e = new MessageEmbed()
				.setTitle('Cuddles :3')
				.setURL(gif.url)
				.setImage(gif.url)
				.setFooter(`Requested by ${msg.author.tag}`, msg.author.displayAvatarURL());
			msg.channel.send(e);
		}
		else {
			const e = new MessageEmbed()
				.setDescription(user === msg.author
					? (`Awww... Don't worry ${msg.author}, i'm here to cuddles you tight!`)
					: (cuddles[Math.floor(Math.random() * cuddles.length)]))
				.setImage(gif.url)
				.setFooter(`Requested by ${msg.author.tag}`, msg.author.displayAvatarURL());
			msg.channel.send(e);
		}
	}
};
