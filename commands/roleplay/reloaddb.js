const { Command } = require('klasa');

module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			aliases: ['db', 'dbuser'],
			cooldown: 5,
			description: 'Reload the databse of a user *if modified for exemple*',
			usage: '[user:user]',
			permissionLevel: 6
		});
	}
	async run(msg, [user = msg.author]) {
		await user.settings.sync([true]);
		msg.channel.send(`Database of ${user.tag} synced.`);
	}

};
