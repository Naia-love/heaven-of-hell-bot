const { Command } = require('klasa');
const client = require('nekos.life');
const { MessageEmbed } = require('discord.js');

module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			aliases: ['pat', 'headpat', 'headpats'],
			description: language => language.get('HELP_PATS'),
      usage: '[user:user]'
		});
	}

  async run(msg, [user]) {
		msg.delete();
    const neko = new client();
    let gif = await neko.sfw.pat();
    if (!user) {
      const e = new MessageEmbed()
        .setTitle('Heapat is the best :3')
        .setURL(gif.url)
        .setImage(gif.url)
        .setFooter(`Requested by ${msg.author.tag}`, msg.author.displayAvatarURL());
      msg.channel.send(e);
    }
    else {
      const e = new MessageEmbed()
        .setDescription(user === msg.author
				      ? (`Here a pat for you ${msg.author}!`)
				      : (`${msg.author} pats the head of ${user}!`))
        .setImage(gif.url)
        .setFooter(`Requested by ${msg.author.tag}`, msg.author.displayAvatarURL());
      msg.channel.send(e);
    };
  }
};
