const { Command } = require('klasa');
const fetch = require('node-fetch');
const { MessageEmbed } = require('discord.js');

module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			aliases: ['gelb'],
			description: language => language.get('HELP_GELBOORU'),
			usage: '[tags:string]',
			nsfw: true
		});
	}

	async run(msg, [tags]) {
		msg.delete();
		if (!tags) {
			const file = await fetch(`https://gelbooru.com/index.php?page=dapi&s=post&q=index&limit=1&tags=sort:random&json=1${process.env.GELBOORU}`)
				.then(response => response.json());

			/*
			 * let data = JSON.parse("response.json");
			 * .then(body => body.file);
			 * msg.channel.send(file["0"].file_url)
			 */
			const e = new MessageEmbed()
				.setTitle('gelbooru')
				.setURL(`https://gelbooru.com/index.php?page=post&s=view&id=${file['0'].id}`)
				.setImage(file['0'].file_url)
				.setFooter(`Requested by ${msg.author.tag}`, msg.author.displayAvatarURL());
			msg.channel.send(e);
		} else {
			const tag = tags.replace(' ', '+');
			let errorc = 0;
			const file = await fetch(`https://gelbooru.com/index.php?page=dapi&s=post&q=index&limit=1&tags=sort:random+${tag}&json=1${process.env.GELBOORU}`)
				.then(response => response.json())
				.catch(error => {
					msg.channel.send('oop, one of your tags/combination of them surely don\'t exist!\nOr an error happened :/');
					errorc++;
				});
			if (errorc < 1) {
				const e = new MessageEmbed()
					.setTitle(tags)
					.setURL(`https://gelbooru.com/index.php?page=post&s=view&id=${file['0'].id}`)
					.setImage(file['0'].file_url)
					.setFooter(`Requested by ${msg.author.tag}`, msg.author.displayAvatarURL());
				msg.channel.send(e);
			}
		}
	}

};
