const { Command } = require('klasa');

module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			aliases: ['modifyaliase'],
			runIn: ['text'],
			description: `modify your aliase! arg= id *modify the OC linked to the aliase* or arg= aliase *replace the aliase*\n(also arg= delete to delete it)`,
			usage: '<aliase:str{1,10}> <arg:str> [modify:str{1,10}]',
			usageDelim: ' '
		});
	}
	async run(msg, [aliase, arg, modify]) {
		await user.settings.sync();
		const aliases = msg.author.settings.get('aliase');
		if (aliases.length === 0) return msg.channel.send('What the hell do you want to modify, you don\'t have aliase dumbass');
		let param = -1;
		for (let i = 0; i < aliases.length; i++) {
			if (aliases[i].aliase === aliase.toLowerCase()) param = i;
		}
		if (param === -1) return msg.channel.send(`You don't have any aliases using \`${aliase}\``);
		if (arg.toLowerCase() === 'id') this.modifyID(msg, [param, modify, aliases]);
		else if (arg.toLowerCase() === 'aliase') this.modifyAliase(msg, [param, modify, aliases]);
		else if (arg.toLowerCase() === 'delete') this.delete(msg, [param, aliase, aliases]);
		else msg.channel.send('Your arg is not an available one');
	}

	async modifyID(msg, [param, modify, aliases]) {
		await msg.author.settings.update('aliase', { oc: modify, aliase: aliases[param].aliase }, { arrayPosition: param });
		msg.channel.send(`your aliase \`${aliases[param].aliase}\` is now usable for your OC with ID \`${modify}\`.`);
	}

	async modifyAliase(msg, [param, modify, aliases]) {
		await msg.author.settings.update('aliase', { oc: aliases[param].oc, aliase: modify }, { arrayPosition: param });
		msg.channel.send(`your aliase \`${aliases[param].aliase}\` is now \`${modify}\`.`);
	}

	async delete(msg, [param, aliase, aliases]) {
		aliases.splice(param, 1);
		await msg.author.settings.update('aliase', aliases, { arrayAction: 'overwrite' });
		msg.channel.send(`Sucefully deleted your aliases ${aliase}!`)
	}
};
