const { Command } = require('klasa');
const client = require('nekos.life');
const { MessageEmbed } = require('discord.js');

module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			aliases: ['solo', 'sologirls', 'sologirlsgif', 'sologirlgif'],
			description: language => language.get('HELP_SOLOGIRL'),
      nsfw: true
		});
	}

  async run(msg) {
		msg.delete();
    const neko = new client();
    let gif = await neko.nsfw.girlSoloGif();
    const e = new MessageEmbed()
      .setTitle("Solo :P")
      .setURL(gif.url)
      .setImage(gif.url)
			.setFooter(`Requested by ${msg.author.tag}`, msg.author.displayAvatarURL())
    msg.channel.send(e);
  }
};
