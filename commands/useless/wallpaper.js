const { Command } = require('klasa');
const client = require('nekos.life');
const { MessageEmbed } = require('discord.js');

module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			aliases: ['wall'],
			description: language => language.get('HELP_WALLPAPER')
		});
	}

  async run(msg) {
    const neko = new client();
    let wall = await neko.sfw.wallpaper();
    const e = new MessageEmbed()
      .setTitle("wallpaper")
      .setURL(wall.url)
      .setImage(wall.url);
    msg.channel.send(e);
  }
};
