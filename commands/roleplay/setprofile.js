const { Command } = require('klasa');

module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			aliases: ['setoc'],
			description: 'set OC of an user (url= the url path of the profile, pfp= the url of the images)',
			usage: '<user:user> <url:url> <pfp:url> <name:...str>',
			usageDelim: ' ',
			runIn: ['text'],
			permissionLevel: 6
		});
	}
	async run(msg, [user, url, pfp, name]) {
		await user.settings.sync();
		user.settings.update('OC', { name, profilePic: pfp, pathToProfile: url }, { arrayAction: 'add' });
		msg.channel.send(`OC for ${user.tag} added!`);
		const i = user.settings.get('OC');
		const id = i.length;
		if (i.length > 1) user.send(`Your oc \`${name}\` (id :\`${id}\`) is approved ! \n Do \`hh!talk ${id} sometext\` to use it :3 \nuse hh!me to verify your OC, contact us if anything :D\nYou can also create aliase ! See \`hh!help aliase\`\nIf you need more help, check <#709813227323785257> !`);
		else user.send(`Your oc \`${name}\` (id :\`${id}\`) is approved ! \n Do \`hh!talk sometext\` to use it :3 \n(use hh!me to verify your OC, contact us if anything :D)\nYou can also create aliase ! See \`hh!help aliase\`\nIf you need more help, check <#709813227323785257> !`);
		msg.delete();
	}

};
